<?php

namespace Drupal\group_storage\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides routes for group_storage group content.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/storage/create');
      $copy->setDefault('base_plugin_id', 'group_storage');
      $collection->add('entity.group_relationship.group_storage_create_page', $copy);
    }

    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/storage/add');
      $copy->setDefault('base_plugin_id', 'group_storage');
      $collection->add('entity.group_relationship.group_storage_add_page', $copy);
    }
  }

}
