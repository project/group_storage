<?php

namespace Drupal\group_storage\Plugin\Group\Relation;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;
use Drupal\storage\Entity\StorageType;

/**
 * Class GroupStorageDeriver extends DeriverBase.
 */
class GroupStorageDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * Get Derivative Definitions.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (StorageType::loadMultiple() as $name => $storage_type) {
      $label = $storage_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', $this->t('Group storage (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', $this->t('Adds %type storage to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
